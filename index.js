// bài 1
function upnum() {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;
  if (num1 > num2 && num1 > num3) {
    if (num2 > num3) {
      document.getElementById(
        "result1"
      ).innerHTML = `Thứ tự tăng dần là: ${num3}>${num2}>${num1}`;
    }
    if (num3 > num2) {
      document.getElementById(
        "result1"
      ).innerHTML = `Thứ tự tăng dần là: ${num2}>${num3}>${num1}`;
    }
  } else if (num2 > num1 && num2 > num3) {
    if (num1 > num3) {
      document.getElementById(
        "result1"
      ).innerHTML = `Thứ tự tăng dần là: ${num3}>${num1}>${num2}`;
    }
    if (num3 > num1) {
      document.getElementById(
        "result1"
      ).innerHTML = `Thứ tự tăng dần là: ${num1}>${num3}>${num2}`;
    }
  } else if (num3 > num2 && num3 > num1) {
    if (num1 > num2) {
      document.getElementById(
        "result1"
      ).innerHTML = `Thứ tự tăng dần là: ${num2}>${num1}>${num3}`;
    }
    if (num2 > num1) {
      document.getElementById(
        "result1"
      ).innerHTML = `Thứ tự tăng dần là: ${num1}>${num2}>${num3}`;
    }
  } else {
    document.getElementById("result1").innerHTML =
      "LỖI! Hãy nhập số và không trùng nhau";
  }
  document.getElementById("result1").style.backgroundColor = "yellow";
  document.getElementById("result1").style.marginTop = "20px";
  document.getElementById("result1").style.padding = "10px";
}
// bài 2
function greeting() {
  var family = document.getElementById("list").value;
  switch (family) {
    case "Chọn thành viên":
      document.getElementById("result2").innerHTML = "xin chào Người Lạ";
      break;
    case "Bố":
      document.getElementById("result2").innerHTML = "xin chào Bố";
      break;
    case "Mẹ":
      document.getElementById("result2").innerHTML = "xin chào Mẹ";
      break;
    case "Anh trai":
      document.getElementById("result2").innerHTML = "xin chào Anh trai";
      break;
    case "Em gái":
      document.getElementById("result2").innerHTML = "xin chào Em gái";
      break;
  }
  document.getElementById("result2").style.backgroundColor = "yellow";
  document.getElementById("result2").style.marginTop = "20px";
  document.getElementById("result2").style.padding = "10px";
}
// bài 3
function count() {
  var num3_1 = document.getElementById("num3_1").value * 1;
  var num3_2 = document.getElementById("num3_2").value * 1;
  var num3_3 = document.getElementById("num3_3").value * 1;
  // console.log(num3_1, num3_2, num3_3);
  var demc = 0;
  if (num3_1 % 2 == 0) {
    demc += 1;
  }
  if (num3_2 % 2 == 0) {
    demc += 1;
  }
  if (num3_3 % 2 == 0) {
    demc += 1;
  }
  var deml = 3 - demc;
  document.getElementById(
    "result3"
  ).innerHTML = `Số Chẳn Là: ${demc}, Số Lẻ Là: ${deml}`;

  document.getElementById("result3").style.backgroundColor = "yellow";
  document.getElementById("result3").style.marginTop = "20px";
  document.getElementById("result3").style.padding = "10px";
}
// bài 4
function guess() {
  var edge1 = document.getElementById("edge1").value * 1;
  var edge2 = document.getElementById("edge2").value * 1;
  var edge3 = document.getElementById("edge3").value * 1;
  var triangle = "";

  if (
    (edge1 === edge2 && edge1 != edge3) ||
    (edge1 === edge3 && edge1 != edge2) ||
    (edge2 === edge3 && edge2 != edge1)
  ) {
    var triangle = "tam giác cân";
  } else if (edge1 === edge2 && edge2 === edge3) {
    var triangle = "tam giác đều";
  } else if (Math.pow(edge3, 2) === Math.pow(edge1, 2) + Math.pow(edge2, 2)) {
    var triangle = "tam giác vuông";
  } else {
    var triangle = "một loại tam giác nào khác";
  }
  document.getElementById("result4").style.backgroundColor = "yellow";
  document.getElementById("result4").style.marginTop = "20px";
  document.getElementById("result4").style.padding = "10px";
  document.getElementById("result4").innerHTML = `${triangle}`;
}
// bài 5
function yday() {
  var day = document.getElementById("day").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  var yesterday = dateyday(day, month, year);

  var result5 = (document.getElementById("result5").innerHTML = dateyday(
    day,
    month,
    year
  ));
}
function tow() {
  var day = document.getElementById("day").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  var towmorow = datetow(day, month, year);
  var result5 = (document.getElementById("result5").innerHTML = datetow(
    day,
    month,
    year
  ));
}
function dateyday(dd, mm, yy) {
  if (mm == 1 && dd >= 1 && dd <= 31) {
    if (dd == 1) {
      var result55 = `ngày 31, tháng 12, năm, ${yy - 1}`;
    } else {
      var result55 = `ngày ${dd - 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 2 && dd >= 1 && dd <= 28) {
    if (dd == 1) {
      var result55 = `ngày 31, tháng ${mm - 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd - 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 3 && dd >= 1 && dd <= 31) {
    if (dd == 1) {
      var result55 = `ngày 28, tháng ${mm - 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd - 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 4 && dd >= 1 && dd <= 30) {
    if (dd == 1) {
      var result55 = `ngày 31, tháng ${mm - 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd - 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 5 && dd >= 1 && dd <= 31) {
    if (dd == 1) {
      var result55 = `ngày 30, tháng ${mm - 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd - 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 6 && dd >= 1 && dd <= 30) {
    if (dd == 1) {
      var result55 = `ngày 31, tháng ${mm - 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd - 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 7 && dd >= 1 && dd <= 31) {
    if (dd == 1) {
      var result55 = `ngày 30, tháng ${mm - 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd - 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 8 && dd >= 1 && dd <= 31) {
    if (dd == 1) {
      var result55 = `ngày 31, tháng ${mm - 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd - 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 9 && dd >= 1 && dd <= 30) {
    if (dd == 1) {
      var result55 = `ngày 31, tháng ${mm - 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd - 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 10 && dd >= 1 && dd <= 31) {
    if (dd == 1) {
      var result55 = `ngày 30, tháng ${mm - 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd - 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 11 && dd >= 1 && dd <= 30) {
    if (dd == 1) {
      var result55 = `ngày 31, tháng ${mm - 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd - 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 12 && dd >= 1 && dd <= 31) {
    if (dd == 1) {
      var result55 = `ngày 30, tháng ${mm - 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd - 1}, tháng ${mm}, năm ${yy}`;
    }
  } else {
    var result55 = "ngày không hợp lệ";
  }
  return result55;
}
function datetow(dd, mm, yy) {
  if (mm == 1 && dd >= 1 && dd <= 31) {
    if (dd == 31) {
      var result55 = `ngày 1, tháng ${mm + 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd + 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 2 && dd >= 1 && dd <= 28) {
    if (dd == 28) {
      var result55 = `ngày 1, tháng ${mm + 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd + 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 3 && dd >= 1 && dd <= 31) {
    if (dd == 31) {
      var result55 = `ngày 1, tháng ${mm + 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd + 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 4 && dd >= 1 && dd <= 30) {
    if (dd == 30) {
      var result55 = `ngày 1, tháng ${mm + 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd + 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 5 && dd >= 1 && dd <= 31) {
    if (dd == 31) {
      var result55 = `ngày 1, tháng ${mm + 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd + 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 6 && dd >= 1 && dd <= 30) {
    if (dd == 30) {
      var result55 = `ngày 1, tháng ${mm + 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd + 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 7 && dd >= 1 && dd <= 31) {
    if (dd == 31) {
      var result55 = `ngày 1, tháng ${mm + 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd + 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 8 && dd >= 1 && dd <= 31) {
    if (dd == 31) {
      var result55 = `ngày 1, tháng ${mm + 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd + 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 9 && dd >= 1 && dd <= 30) {
    if (dd == 30) {
      var result55 = `ngày 1, tháng ${mm + 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd + 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 10 && dd >= 1 && dd <= 31) {
    if (dd == 31) {
      var result55 = `ngày 1, tháng ${mm + 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd + 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 11 && dd >= 1 && dd <= 30) {
    if (dd == 30) {
      var result55 = `ngày 1, tháng ${mm + 1}, năm ${yy}`;
    } else {
      var result55 = `ngày ${dd + 1}, tháng ${mm}, năm ${yy}`;
    }
  } else if (mm == 12 && dd >= 1 && dd <= 31) {
    if (dd == 31) {
      var result55 = `ngày 1, tháng 1, năm, ${yy + 1}`;
    } else {
      var result55 = `ngày ${dd + 1}, tháng ${mm}, năm ${yy}`;
    }
  } else {
    var result55 = "ngày không hợp lệ";
  }
  return result55;
}
// bài 6
function leap() {
  var monthLeap = document.getElementById("monthleap").value * 1;
  var yearLeap = document.getElementById("yearleap").value * 1;
  var result6 = document.getElementById("result6");
  if (monthLeap >= 1 && monthLeap <= 12) {
    if (
      (monthLeap == 2 && yearLeap % 4 == 0 && yearLeap % 100 !== 0) ||
      (yearLeap % 400 == 0 && monthLeap == 2)
    ) {
      result6.innerText = "29 ngày";
    } else {
      switch (monthLeap) {
        case 1:
          result6.innerText = "31 ngày";
          break;
        case 2:
          result6.innerText = "28 ngày";
          break;
        case 3:
          result6.innerText = "31 ngày";
          break;
        case 4:
          result6.innerText = "30 ngày";
          break;
        case 5:
          result6.innerText = "31 ngày";
          break;
        case 6:
          result6.innerText = "30 ngày";
          break;
        case 7:
          result6.innerText = "31 ngày";
          break;
        case 8:
          result6.innerText = "31 ngày";
          break;
        case 9:
          result6.innerText = "30 ngày";
          break;
        case 10:
          result6.innerText = "31 ngày";
          break;
        case 11:
          result6.innerText = "30 ngày";
          break;
        case 12:
          result6.innerText = "31 ngày";
          break;
      }
    }
  } else {
    result6.innerText = "Không có tháng này ";
  }
}
// bài 7
function reak() {
  var numReak = document.getElementById("numberreak").value;
  var numR1 = numReak.charAt(0) * 1;
  var numR2 = numReak.charAt(1) * 1;
  var numR3 = numReak.charAt(2) * 1;

  // console.log(result7);
  var result7 = (document.getElementById("result7").innerHTML = reakDemo(
    numR1,
    numR2,
    numR3
  ));
  function reakDemo(R1, R2, R3) {
    switch (R1) {
      case 0:
        var result77 = ``;
        break;
      case 1:
        var result77 = ` một trăm`;
        break;
      case 2:
        var result77 = ` hai trăm`;
        break;
      case 3:
        var result77 = ` ba trăm`;
        break;
      case 4:
        var result77 = ` bốn trăm`;
        break;
      case 5:
        var result77 = ` năm trăm`;
        break;
      case 6:
        var result77 = ` sáu trăm`;
        break;
      case 7:
        var result77 = ` bảy trăm`;
        break;
      case 8:
        var result77 = ` tám trăm`;
        break;
      case 9:
        var result77 = ` chín trăm`;
        break;
      default:
        alert("số đầu tiên không phải là số");
    }
    switch (R2) {
      case 0:
        var result78 = ` lẻ`;
        break;
      case 1:
        var result78 = ` mười`;
        break;
      case 2:
        var result78 = ` hai mươi`;
        break;
      case 3:
        var result78 = ` ba mươi`;
        break;
      case 4:
        var result78 = ` bốn mươi`;
        break;
      case 5:
        var result78 = ` năm mươi`;
        break;
      case 6:
        var result78 = ` sáu mươi`;
        break;
      case 7:
        var result78 = ` bảy mươi`;
        break;
      case 8:
        var result78 = ` tám mươi`;
        break;
      case 9:
        var result78 = ` chín mươi`;
        break;
    }
    switch (R3) {
      case 0:
        var result79 = ` `;
        break;
      case 1:
        var result79 = ` một`;
        break;
      case 2:
        var result79 = ` hai`;
        break;
      case 3:
        var result79 = ` ba`;
        break;
      case 4:
        var result79 = ` bốn`;
        break;
      case 5:
        var result79 = ` năm`;
        break;
      case 6:
        var result79 = ` sáu`;
        break;
      case 7:
        var result79 = ` bảy`;
        break;
      case 8:
        var result79 = ` tám`;
        break;
      case 9:
        var result79 = ` chín`;
        break;
    }
    var tong = result77 + result78 + result79;
    return tong;
  }
}
// bài 8
function coordinates() {
  var namesv1 = document.getElementById("namesv1").value;
  var xsv1 = document.getElementById("xsv1").value * 1;
  var ysv1 = document.getElementById("ysv1").value * 1;
  var namesv2 = document.getElementById("namesv2").value;
  var xsv2 = document.getElementById("xsv2").value * 1;
  var ysv2 = document.getElementById("ysv2").value * 1;
  var namesv3 = document.getElementById("namesv3").value;
  var xsv3 = document.getElementById("xsv3").value * 1;
  var ysv3 = document.getElementById("ysv3").value * 1;
  var xT = document.getElementById("xT").value * 1;
  var yT = document.getElementById("yT").value * 1;
  var tsv1 = coor(xT, yT, xsv1, ysv1);
  var tsv2 = coor(xT, yT, xsv2, ysv2);
  var tsv3 = coor(xT, yT, xsv3, ysv3);
  if (tsv1 >= tsv2 && tsv1 >= tsv2) {
    var far = namesv1;
  } else if (tsv2 >= tsv1 && tsv2 >= tsv3) {
    var far = namesv2;
  } else if (tsv3 >= tsv1 && tsv3 >= tsv2) {
    var far = namesv3;
  }
  document.getElementById(
    "result8"
  ).innerText = `Học sinh xa trường nhất là: ${far}`;
}
function coor(x, y, x1, y1) {
  var Ab1 = x1 - x;
  var Ab2 = y1 - y;
  var AB = Math.sqrt(Math.pow(Ab1, 2) + Math.pow(Ab2, 2));
  return AB;
}
